<?php

namespace Mojomaja\Bundle\MaximBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mojomaja\Component\Maxim;

class TouchCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('maxim:touch')
            ->setDescription('Set up transient relationship')
            ->addArgument('from', InputArgument::REQUIRED, 'from, a token')
            ->addArgument('to', InputArgument::REQUIRED, 'user, an id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $maxim = $this->getContainer()->get('mojomaja_maxim.maxim');
        $maxim->touch($input->getArgument('to'), $input->getArgument('from'));

        $output->writeln('ok.');
    }
}
