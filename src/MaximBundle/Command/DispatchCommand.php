<?php

namespace Mojomaja\Bundle\MaximBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mojomaja\Component\Maxim;

class DispatchCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('maxim:dispatch')
            ->setDescription('Dispatch maxim message')
            ->addArgument('from', InputArgument::REQUIRED, 'sender, a token')
            ->addArgument('to', InputArgument::REQUIRED, 'receiver, an id')
            ->addOption('group', 'g', InputOption::VALUE_REQUIRED, 'receiver, is a group')
            ->addOption('text', null, InputOption::VALUE_REQUIRED)
            ->addOption('image', null, InputOption::VALUE_REQUIRED, 'image, a path')
            ->addOption('audio', null, InputOption::VALUE_REQUIRED, 'audio, a path')
            ->addOption('meta', null, InputOption::VALUE_REQUIRED, 'meta, a json')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $maxim = $this->getContainer()->get('mojomaja_maxim.maxim');
        $maxim->dispatch(
            new Maxim\Message([
                'text'  => $input->getOption('text'),
                'image' => $input->getOption('image'),
                'audio' => $input->getOption('audio'),
                'meta'  => json_decode($input->getOption('meta')),
                'token' => $input->getArgument('from')
            ]),
            $input->getArgument('to'),
            $input->getOption('group') ? Maxim\Client::TYPE_GROUP : Maxim\Client::TYPE_USER
        );

        $output->writeln('ok.');
    }
}
