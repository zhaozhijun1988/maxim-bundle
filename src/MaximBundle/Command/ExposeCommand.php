<?php

namespace Mojomaja\Bundle\MaximBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mojomaja\Component\Maxim;

class ExposeCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('maxim:expose')
            ->setDescription('Expose contacts')
            ->addArgument('to', InputArgument::REQUIRED, 'user, an id')
            ->addArgument('contacts', InputArgument::IS_ARRAY, 'contacts, id:name pairs')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $maxim = $this->getContainer()->get('mojomaja_maxim.maxim');
        $maxim->expose(
            $input->getArgument('to'),
            array_map(function ($contact) {
                return explode(':', $contact, 2);
            }, $input->getArgument('contacts'))
        );

        $output->writeln('ok.');
    }
}
