<?php

namespace Mojomaja\Bundle\MaximBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mojomaja\Component\Maxim;

class ChallengeCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('maxim:challenge')
            ->setDescription('Dispatch service call')
            ->addArgument('referer', InputArgument::REQUIRED, 'referer id')
            ->addArgument('from', InputArgument::REQUIRED, 'sender, a token')
            ->addArgument('to', InputArgument::IS_ARRAY, 'receiver, service id(s)')
            ->addOption('text', null, InputOption::VALUE_REQUIRED)
            ->addOption('image', null, InputOption::VALUE_REQUIRED, 'image, a path')
            ->addOption('audio', null, InputOption::VALUE_REQUIRED, 'audio, a path')
            ->addOption('meta', null, InputOption::VALUE_REQUIRED, 'meta, a json')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $maxim = $this->getContainer()->get('mojomaja_maxim.maxim');
        $maxim->challenge(
            $input->getArgument('to'),
            new Maxim\Message([
                'text'  => $input->getOption('text'),
                'image' => $input->getOption('image'),
                'audio' => $input->getOption('audio'),
                'meta'  => json_decode($input->getOption('meta')),
                'token' => $input->getArgument('from')
            ]),
            $input->getArgument('referer')
        );

        $output->writeln('ok.');
    }
}
